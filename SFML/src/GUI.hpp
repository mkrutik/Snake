#ifndef GUI_HPP
#define GUI_HPP

#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include <SFML/System.hpp>

#include "../../game/src/IGUI.hpp"

#define HEAD 1
#define BODY 2

#define F_YELLOW 3 // food
#define F_BLUE 4 // food
#define F_MAGENTA 5 // food

#define CYAN 6 // food

#define OBSTICAL 7 // obstical

#define CYAN_T 8
#define INFO_LINE 9 // info line

#define RED_T 10 // for text
#define GREEN_T 11 // for text
#define BLUE_T 12 // for text

class GUI : public IGUI
{
private:
    sf::RenderWindow    _w;
    const int           _widht;
    const int           _height;
    float               _speed;
    sf::Texture         _array[8];
    sf::Font            _font;

    GUI(const GUI &src);
    GUI& operator = (const GUI &src);

    void sprite_draw(int id, int x, int y);
    void text_draw(int color, int x, int y, const char *data);

public:
    GUI(int x, int y, float speed);
    ~GUI();

    int     pressed_button();
    void    draw(std::vector<std::pair< std::pair<int, const char*>, std::pair<int, int> > > src);
    void    getXY(int &x, int &y);
    void    change_speed(void);
};

extern "C" {
    IGUI*    createGui(int x, int y, float speed);
    void    destroieGui(IGUI *src);
}

#endif