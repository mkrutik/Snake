#include "GUI.hpp"

GUI::GUI(int x, int y, float speed) : _w(sf::VideoMode((x * 40 + 80), (y * 40) + 104), "Snake - sfml", sf::Style::Titlebar), _widht(x*40 + 80), _height(y * 40 + 4), _speed(speed)
{
    _w.setActive();
    _w.setKeyRepeatEnabled(false);

    sf::Vector2i pos( (sf::VideoMode::getDesktopMode().width / 2)- _widht / 2, (sf::VideoMode::getDesktopMode().height / 2) - _height / 2);
    _w.setPosition(pos);
    _w.setVerticalSyncEnabled(true);
    _w.setFramerateLimit(30);
    _w.setMouseCursorVisible(false);


    _array[0].loadFromFile("REPLACE_PATH/SFML/src/image/head.png", sf::IntRect(0, 0, 40, 40));
    _array[1].loadFromFile("REPLACE_PATH/SFML/src/image/body.png", sf::IntRect(0, 0, 40, 40));

    _array[2].loadFromFile("REPLACE_PATH/SFML/src/image/food_1.png", sf::IntRect(0, 0, 40, 40));
    _array[3].loadFromFile("REPLACE_PATH/SFML/src/image/food_2.png", sf::IntRect(0, 0, 40, 40));
    _array[4].loadFromFile("REPLACE_PATH/SFML/src/image/food_3.png", sf::IntRect(0, 0, 40, 40));
    _array[5].loadFromFile("REPLACE_PATH/SFML/src/image/food_4.png", sf::IntRect(0, 0, 40, 40));
    
    _array[6].loadFromFile("REPLACE_PATH/SFML/src/image/obstical.png", sf::IntRect(0, 0, 40, 40));
    _array[7].loadFromFile("REPLACE_PATH/SFML/src/image/bacground.jpg", sf::IntRect(40, 64, x * 40, y*40));

    _font.loadFromFile("REPLACE_PATH/SFML/src/FONT.ttf");
}

GUI::~GUI()
{
    _w.close();
}

int     GUI::pressed_button()
{
    sf::Clock clock;
    sf::Event event;
    _w.pollEvent(event);

    while (1)
    {
        if (event.type == sf::Event::KeyPressed)
        {
            if (event.key.code == sf::Keyboard::Escape)
                return 'q';
            else if (event.key.code == sf::Keyboard::Q)
                return 'q';
            else if (event.key.code == sf::Keyboard::Right)
                return 1;
            else if (event.key.code == sf::Keyboard::Left) 
                return 0;
            else if (event.key.code == sf::Keyboard::R)
                return 'r';
            else if (event.key.code == sf::Keyboard::P)
                return 'p';
            else if (event.key.code == sf::Keyboard::Num1)
                return '1';
            else if (event.key.code == sf::Keyboard::Num2)
                return '2';
            else if (event.key.code == sf::Keyboard::Num3)
                return '3';
            else if (event.key.code == sf::Keyboard::N)
                return 'n';
        }

        if (clock.getElapsedTime().asMilliseconds() >= (75 * _speed))
            return -1;
        _w.display();
    }
}


void GUI::sprite_draw(int id, int x, int y)
{
    sf::Sprite tmp;
    tmp.setTexture(_array[id - 1]);
    tmp.setPosition(x,y);
    _w.draw(tmp);
}

void    GUI::text_draw(int color, int x, int y, const char *data)
{
    sf::Color array[4];
    array[0] = sf::Color::Cyan;
    array[1] = sf::Color::Red;
    array[2] = sf::Color::Green;
    array[3] = sf::Color::Blue;

    sf::Text text(data, _font);
    text.setPosition(x, y);
    text.setCharacterSize(24);
    text.setColor(array[color]);

    _w.draw(text);
}

void    GUI::draw(std::vector<std::pair< std::pair<int, const char*>, std::pair<int, int> > > src)
{
    _w.clear();

    sprite_draw(8, 40, 64);

    for (unsigned long i = 0; i < src.size(); i++)
    {
        int id = src[i].first.first;

        if (id == 9 || id == 10 || id == 11 || id == 12) // text
        {
            std::string tmp(src[i].first.second);
            int x = _widht / 2 - (tmp.size() * 14) / 2; 
            text_draw(src[i].first.first - 9, x , src[i].second.second * 40, src[i].first.second);
        }
        else // spites
            sprite_draw(id, src[i].second.first * 40 + 40, src[i].second.second * 40 + 54);
    }

    _w.display();
}
void    GUI::getXY(int &x, int &y)
{
    x = _w.getSize().x;
    y = _w.getSize().y;
}

void    GUI::change_speed(void)
{
    _speed -= 0.5;
}

IGUI*    createGui(int x, int y, float speed)
{
    return new GUI(x, y, speed);
}

void    destroieGui(IGUI *src)
{
    delete src;
}