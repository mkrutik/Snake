#include "header.h"

// if snake head contact with food, eat it and delete this food 
void    check_contact_with_food(game &data)
{
    std::pair<std::pair<int, const char*>, std::pair<int, int> >  tmp = data.zmei->head_pos();
    int head_x = tmp.second.first;
    int head_y = tmp.second.second;

    for (unsigned long i = 0; i < data.food.size(); i++)
    {
        if (head_x == data.food[i].second.first && head_y == data.food[i].second.second)
        {
            data.score += 10;
            data.change_score = true;
            data.zmei->eat();
            data.food.erase(data.food.begin() + i);
            break;
        }
    }
}

// check situation when snake going beyond the window
bool    going_beyond_window(Zmei &src, int max_x, int max_y)
{
    std::pair<std::pair<int, const char*>, std::pair<int, int> >  tmp = src.head_pos();
    if (tmp.second.first < 0 || tmp.second.first >= max_x || tmp.second.second < 0 || tmp.second.second >= max_y)
        return true;
    return false;
}

bool    check_with_obsticals(game &data)
{
    std::pair<std::pair<int, const char*>, std::pair<int, int> >  head = data.zmei->head_pos();

    for (unsigned long i = 0; i < data.obstical.size(); i++)
    {
        if (head.second.first == data.obstical[i].second.first && head.second.second == data.obstical[i].second.second)
            return true;
    }
    return false;
}

bool    check_eat_yourself(Zmei &src)
{
    std::pair<std::pair<int, const char*>, std::pair<int, int> >  tmp = src.head_pos();
    std::vector<std::pair<std::pair<int, const char*>, std::pair<int, int> > > d = src.forPrint();
    for (unsigned long i = 1; i < d.size(); i++)
    {
        if (tmp.second.first == d[i].second.first && tmp.second.second == d[i].second.second)
            return true;
    }
    return false;
}

// check for resize window
bool resize(const game &data, int cur_x, int cur_y)
{
    if (data.win_w != cur_x || data.win_h != cur_y)
        return true;
    return false;
}