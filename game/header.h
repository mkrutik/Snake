#ifndef HEADER_H
#define HEADER_H

#include <iostream>
#include <vector>
#include <string>
#include <regex>
#include <cstdlib> // for exit()
#include <dlfcn.h> // for work with dll

#include "IGUI.hpp"
#include "Zmei.hpp"


#define MIN_X 30
#define MAX_X 70

#define MIN_Y 30
#define MAX_Y 70

struct game
{
    unsigned int win_w;
    unsigned int win_h;

    unsigned int max_x; // for snake
    unsigned int max_y; // for snake

    Zmei            *zmei;
    unsigned int    score;
    bool            change_score;
    std::string     name;
    std::string     *top_line;

    std::vector< std::pair< std::pair< int, const char*>, std::pair<int, int> > > food;
    std::vector< std::pair< std::pair< int, const char*>, std::pair<int, int> > > obstical;    
        
    void *handle; // dll handler

    int gui_id; // curent gui
    IGUI *g;
    void *(*destroieGui)(IGUI *src);
};

void    error_handler(std::string src);
void    algorithm(game &data);


void    check_contact_with_food(game &data);
bool    going_beyond_window(Zmei &src, int max_x, int max_y);
bool    check_with_obsticals(game &data);
bool    check_eat_yourself(Zmei &src);
bool    resize(const game &data, int cur_x, int cur_y);

void    generate_food(game &data);
void    genere_obstical(game &data);

void create_gui(int n, game &data);



#endif