#include "header.h"

void    generate_food(game &data)
{
    std::srand(0);
    int n_food = 1 + rand() % 10;
    int x;
    int y;

    bool f;

    for (; n_food != 0 && data.food.size() < 10;)
    {
        f = false;
        x = rand() % data.max_x;
        y = rand() % data.max_y;

        std::vector<std::pair<std::pair<int, const char*>, std::pair<int, int> > > tmp = data.zmei->forPrint();
        
        for (unsigned long i = 0; i < data.food.size(); i++)
        {
            if (x == data.food[i].second.first && y == data.food[i].second.second)
                break;
            else if (i + 1 == data.food.size())
                f = true;
        }
        
        if (f || data.food.size() == 0)
            for (int j = 0; j < tmp.size(); j++)
            {
                if (x == tmp[j].second.first && y == tmp[j].second.second)
                    break ;
                else if (j + 1 == tmp.size())
                {
                    int tmp = 2 + rand() % 4;
                    data.food.push_back( std::make_pair(std::make_pair(tmp, "$"), std::make_pair(x,y)));
                    n_food--;
                }
            }
    }
}

void    genere_obstical(game &data)
{
    int num = 1 + rand() % 9;
    int x;
    int y;

    bool f;
    for (; num != 0 ;)
    {
        f = false;
        x = rand() % data.max_x;
        y = rand() % data.max_y;

        std::vector<std::pair<std::pair<int, const char*>, std::pair<int, int> > > tmp = data.zmei->forPrint();
        
        for (unsigned long i = 0; i < data.food.size(); i++)
        {
            if (x == data.food[i].second.first && y == data.food[i].second.second)
                break;
            else if (i + 1 == data.food.size())
                f = true;
        }
        
        if (f)
        {
            std::pair<std::pair<int, const char*>, std::pair<int, int> >  head = data.zmei->head_pos();
            if (x != head.second.first && y != head.second.second &&
                (x + 1) != head.second.first && (x -1) != head.second.first && 
                (y + 1) != head.second.second && (y - 1) != head.second.second)
            {
                for (int j = 1; j < tmp.size(); j++)
                {
                    if (x == tmp[j].second.first && y == tmp[j].second.second)
                        break ;
                    else if (j + 1 == tmp.size())
                    {
                        data.obstical.push_back( std::make_pair(std::make_pair(7, "X"), std::make_pair(x,y)));
                        num--;
                    }
                
                }
            }
        }
    }
}