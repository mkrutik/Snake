#include "header.h"

std::vector<std::pair< std::pair<int, const char*>, std::pair<int, int> > >  prepare_data(const game &data)
{
    std::vector<std::pair< std::pair<int, const char*>, std::pair<int, int> > > res = data.zmei->forPrint();

    res.insert(res.end(), data.food.begin(), data.food.end());

    if (data.obstical.size() != 0)
        res.insert(res.end(), data.obstical.begin(), data.obstical.end());
    
    std::vector<std::pair< std::pair<int, const char*>, std::pair<int, int> > > tmp;
    for (unsigned long i = 0; i < res.size(); i++)
    {
        res[i].second.second += 1; // one line for frame 
        if (data.gui_id == 0)
        {
            res[i].second.first *= 2;
            tmp.push_back(std::make_pair(std::make_pair(res[i].first.first, res[i].first.second), std::make_pair(res[i].second.first + 1, res[i].second.second) ));
        }
    }
    if (data.gui_id == 0)
        res.insert(res.end(), tmp.begin(), tmp.end());
    
    data.top_line->clear();
    *data.top_line = data.name + std::to_string(data.score);
    int i = (data.gui_id == 0) ? (data.max_x * 2) : data.win_w;
    i -= data.top_line->size();
    bool f = false;
    while (i)
    {
        if (f)
        {
            f = false;
            *data.top_line += " ";
        }
        else
        {
            f = true;
            *data.top_line = " " + *data.top_line;
        }
        --i;
    }

    res.push_back(std::make_pair (std::make_pair(9, static_cast<const char*>(data.top_line->c_str() )), std::make_pair( 0, 0) ) );
    
    return res;
}

void create_gui(int n, game &data)
{
    static std::string names[] = {"lib1.so", "lib2.so", "lib3.so"};   
    
    if (n == data.gui_id)
        return ;
    
    data.gui_id = n;
    if (data.g != NULL)
        data.destroieGui(data.g);
    if (data.handle != NULL)
        dlclose(data.handle);


    data.handle = dlopen(names[n].c_str(), RTLD_LAZY | RTLD_LOCAL);
    if (!data.handle)
        error_handler( ("Can't find library" + names[n]) );

    IGUI *(*createGui)(int, int, float);
    createGui = (IGUI *(*)(int, int, float))dlsym(data.handle, "createGui");

    if (createGui == NULL)
    {
        dlclose(data.handle);
        error_handler( ("Can't find function createGui in this library " + names[n]) );
    }
    if (data.gui_id == 0)
        data.g = createGui((data.max_x * 2 + 2), data.max_y, 5.0);    
    else
        data.g = createGui(data.win_w, data.win_h, 5.0);    
    data.destroieGui = (void *(*)(IGUI *src))dlsym(data.handle, "destroieGui");
    
    if (data.destroieGui == NULL)
    {
        dlclose(data.handle);
        error_handler( ("Can't find function destroieGui in this library " + names[n]) );
    }
}

void exit(game &data)
{
    data.destroieGui(data.g);
    dlclose(data.handle);
    delete data.zmei;
    exit(1);
}

void    pause_win(game &data)
{
    int key_code;
    std::vector<std::pair< std::pair<int, const char*>, std::pair<int, int> > > res;
    int x = (data.win_w - 24)/ 2;
    int y = data.win_h / 2 - 1;
    res.push_back(std::make_pair(std::make_pair(11, "[r] - return to the game"), std::make_pair(x, y)));
    res.push_back(std::make_pair(std::make_pair(10, "[q] - quite the game"), std::make_pair(x, y+1)));
    res.push_back(std::make_pair(std::make_pair(12, "[1],[2],[3]-swich between gui"), std::make_pair(x, y+2)));

    while (1)
    {
        if ((key_code = data.g->pressed_button()) == 'q')
            exit(data);
        else if (key_code == 'r') // pause
            break ;
        else if (key_code == '1') // first lib
            create_gui(0, data);
        else if (key_code == '2') // second lib
            create_gui(1, data);
        else if (key_code == '3') // third lib
            create_gui(2, data);

        data.g->draw(res);
        key_code = -1;
    }
}

void    game_over(game &data)
{
    std::vector<std::pair< std::pair<int, const char*>, std::pair<int, int> > > res;
    std::string print = "GAME OVER";
    int key_code;
    int x = 6;
    int y = data.win_h / 2 - 1;
    std::string *tmp = new std::string("Your result: " + std::to_string(data.score));
    res.push_back(std::make_pair(std::make_pair(10, "*** GAME OVER ***"), std::make_pair(x, y)));
    res.push_back(std::make_pair(std::make_pair(11, static_cast<const char*>(tmp->c_str())), std::make_pair(x, y + 1)));
    res.push_back(std::make_pair(std::make_pair(11, "[n] - start new game"), std::make_pair(x, y + 4)));
    res.push_back(std::make_pair(std::make_pair(10, "[q] - quite the game"), std::make_pair(x, y + 5)));

    while (1)
    {
        if ((key_code = data.g->pressed_button()) == 'q')
        {
            delete tmp;
            exit(data);
        }
        else if (key_code == 'n') // pause
            break;
        data.g->draw(res);
        key_code = -1;
    }

    delete tmp;
    delete data.zmei;
    data.zmei = new Zmei( (data.max_x/2 - 2) , (data.max_y/2));
    data.score = 0;
    data.change_score = false;
    data.food.clear();
    data.obstical.clear();
    int n = data.gui_id;
    data.gui_id = -1;
    create_gui(n, data);
    generate_food(data);
    algorithm(data);
}

void    algorithm(game &data)
{
    int cur_x;
    int cur_y;
    int key_code = -1;

    while (1)
    {
        if ((key_code = data.g->pressed_button()) == 'q')
            exit(data);
        else if (key_code == 0)
            data.zmei->left();
        else if (key_code == 1)
            data.zmei->right();
        else if (key_code == 'p') // pause
            pause_win(data);
        else if (key_code == '1') // first lib
            create_gui(0, data);
        // else if (key_code == '2') // second lib
        //     create_gui(1, data);
        // else if (key_code == '3') // third lib
        //     create_gui(2, data);

        if (data.food.size() < 4)
            generate_food(data);

        // data.g->getXY(cur_x, cur_y);
        // if (resize(data, cur_x, cur_y))
        // {
        //     delete data.zmei;
        //     dlclose(data.handle);
        //     error_handler("Resize window unsupported!");
        // }

        data.zmei->move();
        check_contact_with_food(data);
        
        
        if (going_beyond_window(*data.zmei, data.max_x , data.max_y ) || check_eat_yourself(*data.zmei) || (data.obstical.size() != 0 && check_with_obsticals(data)))
            return game_over(data);

        // if (check_eat_yourself(*data.zmei))
        // {
        // }

        // if (data.obstical.size() != 0 && check_with_obsticals(data))
        // {
        // }

        data.g->draw(prepare_data(data));
        if (data.change_score == true && (data.score % 100) == 0)
            data.g->change_speed();
        if (data.change_score == true && data.score % 200 == 0)
            genere_obstical(data);
        key_code = -1;
        data.change_score = false;
    }
}