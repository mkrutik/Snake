#ifndef ZMEI_HPP
#define ZMEI_HPP

#include <vector>
#include <queue>
#include <utility> // pair

//            up 0
//   left 3  < ^ > right 1
//             v
//           down 2

class Zmei 
{    
private:
    std::vector<std::pair<std::pair<int, const char*>, std::pair<int, int> > >  _data;
    int                 _direction; // up, down, right, left
    std::queue<std::pair<std::pair<int, const char*>, std::pair<int, int> > >   _eat;

    Zmei(const Zmei &src);
    Zmei& operator = (const Zmei &src);

public:
    Zmei(const int x, const int y); // start position
    ~Zmei();

    const std::vector<std::pair<std::pair<int, const char*>, std::pair<int, int> > >&   forPrint(void) const;
    const std::pair<std::pair<int, const char*>, std::pair<int, int> >&                head_pos(void) const;
    
    void                        move(void);

    void                        left(void);
    void                        right(void);
    void                        eat(void);
};

#endif