#include "header.h"

void usage(void)
{
    std::cout << "Usage: ./snake [field with (" << MIN_X << " < with < " << MAX_X << ")] [field height ( "
              << MIN_Y << "< height < " << MAX_Y << ")] [player name ( [A-Za-z0-9]{15} )]" << std::endl;
}

void    error_handler(std::string src)
{
    std::cout << "ERROR: " << src << std::endl << "Please try again." << std::endl;
    usage();
    exit(-1); 
}

void    parse_arguments(game &data, char **argv)
{
    std::string max = std::to_string(MAX_X);
    std::string min = std::to_string(MIN_X);
    std::string for_reg = "[0-9]{" + std::to_string(min.size()) + "," + std::to_string(max.size()) + "}"; 
    std::regex w( (const_cast<char*>(for_reg.c_str())) );
    if ( !std::regex_match(argv[1], w) || (data.win_w = std::stoi(argv[1])) < MIN_X || data.win_w > MAX_X)
        error_handler("Damaged with argument!");
    for_reg.clear();

    max = std::to_string(MAX_Y);
    min = std::to_string(MIN_Y);
    for_reg = "[0-9]{" + std::to_string(min.size()) + "," + std::to_string(max.size()) + "}"; 
    std::regex h( (const_cast<char*>(for_reg.c_str())) );
    if ( !std::regex_match(argv[2], h) || (data.win_h = std::stoi(argv[2])) < MIN_Y || data.win_h > MAX_Y)
        error_handler("Damaged height argument!");
    
    std::regex n("[A-Za-z0-9]{1,10}");
    if ( !std::regex_match(argv[3], n))
        error_handler("Damaged name argument!");
    data.name = "Name: " + std::string(argv[3]) + " | Score: ";
}

void initialize(game &src, int gui_id)
{
    src.zmei = new Zmei( src.win_w / 2 - 2 , src.win_h / 2);
    src.score = 0;
    src.change_score = false;
    src.gui_id = -1;
    src.handle = NULL;
    src.top_line = new std::string();
    src.g = NULL;
    generate_food(src);
    create_gui(gui_id, src);
}

int main(int argc, char **argv)
{
    if (argc != 4)
        error_handler("Not enough arguments!");
    
    game data;
    parse_arguments(data, argv);
    initialize(data, 0);
    algorithm(data);

    return (0);
}