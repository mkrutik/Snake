#ifndef HEADER_H
#define HEADER_H

#include <iostream>
#include <vector>
#include <string>
#include <regex>
#include <cstdlib> // for exit()
#include <dlfcn.h> // for work with dll

#include "IGUI.hpp"
#include "Zmei.hpp"


#ifdef __APPLE__
   #define LIB1 "lib1.dylib"
   #define LIB2 "lib2.dylib"
   #define LIB3 "lib3.dylib"
#elif __linux__
    #define LIB1 "lib1.so"
    #define LIB2 "lib2.so"
    #define LIB3 "lib3.so"
#endif




#define MIN_X 20
#define MAX_X 100

#define MIN_Y 20
#define MAX_Y 100

struct game
{
    unsigned int win_w;
    unsigned int win_h;

    Zmei            *zmei;
    unsigned int    score;
    bool            change_score;
    std::string     name;
    std::string     *top_line;

    std::vector< std::pair< std::pair< int, const char*>, std::pair<int, int> > > food;
    std::vector< std::pair< std::pair< int, const char*>, std::pair<int, int> > > obstical;    
        
    void *handle; // dll handler

    int gui_id; // curent gui
    IGUI *g;
    void *(*destroieGui)(IGUI *src);
};

void    error_handler(std::string src);
void    algorithm(game &data);

void    check_contact_with_food(game &data);
bool    going_beyond_window(Zmei &src, int max_x, int max_y);
bool    check_with_obsticals(game &data);
bool    check_eat_yourself(Zmei &src);
bool    resize(const game &data, int cur_x, int cur_y);

void    generate_food(game &data);
void    genere_obstical(game &data);

void create_gui(int n, game &data);

#endif