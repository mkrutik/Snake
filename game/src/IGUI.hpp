#ifndef IGUI_HPP
#define IGUI_HPP

#include <vector>
#include <utility>
#include <string>

class IGUI 
{
// protected:
//     typedef std::vector<std::pair<int, const char*>, std::pair<int, int> > print;

public: 
    virtual ~IGUI() {}

    virtual int     pressed_button() = 0;
    virtual void    draw(std::vector<std::pair< std::pair<int, const char*>, std::pair<int, int> > > src) = 0;
    virtual void    getXY(int &x, int &y) = 0;
    virtual void    change_speed(void) = 0;
};

#endif