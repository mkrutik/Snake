#include "Zmei.hpp"

Zmei::Zmei(const int x, const int y) : _data(), _direction(3), _eat()
{
    _data.push_back( std::make_pair(std::make_pair(1, "*" ), std::make_pair(x, y)));
    _data.push_back( std::make_pair(std::make_pair(2, " " ), std::make_pair((x + 1), y)));
    _data.push_back( std::make_pair(std::make_pair(2, " " ), std::make_pair((x + 2), y)));
    _data.push_back( std::make_pair(std::make_pair(2, " " ),  std::make_pair((x + 3), y)));
}

Zmei::~Zmei() {}

const std::vector<std::pair<std::pair<int, const char*>, std::pair<int, int> > >&     Zmei::forPrint(void) const
{
    return _data;
}
const std::pair<std::pair<int, const char*>, std::pair<int, int> > &            Zmei::head_pos(void) const
{
    return _data[0];
}

void                    Zmei::move(void)
{
    int x_next;
    int y_next;
    int x_curent;
    int y_curent;

    for (unsigned long i = 0; i < _data.size(); i++)
    {
        if (i == 0)
        {
            x_next = _data[i].second.first;
            y_next = _data[i].second.second;
            if (_direction == 0)
                _data[i].second.second = y_next - 1;
            else if (_direction == 1)
                _data[i].second.first = x_next + 1;
            else if (_direction == 2)
                _data[i].second.second = y_next + 1;
            else if (_direction == 3)
                _data[i].second.first = x_next - 1;
        }
        else
        {
            x_curent = x_next;
            y_curent = y_next;
            x_next = _data[i].second.first;
            y_next = _data[i].second.second;
            _data[i].second.first = x_curent;
            _data[i].second.second = y_curent;
        }
    }
    if ( _eat.size() > 0 && x_next == _eat.front().second.first && y_next == _eat.front().second.second)
    {
        _data.push_back(_eat.front());
        _eat.pop();
    }
}

void                    Zmei::left(void)
{
    _direction--;
    if (_direction < 0)
        _direction = 3;
}
void                    Zmei::right(void)
{
    _direction++;
    if (_direction > 3)
        _direction = 0;
}

void    Zmei::eat(void)
{
    _eat.push( std::make_pair(std::make_pair(2, " "), std::make_pair(_data[0].second.first, _data[0].second.second)));
}
