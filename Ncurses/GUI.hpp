#ifndef GUI_HPP
#define GUI_HPP

#include <ncurses.h>
#include "../game/IGUI.hpp"

// bacground color definition with White text color 
#define RED 1
#define GREEN 2 
#define YELLOW 3
#define BLUE 4
#define MAGENTA 5
#define CYAN 6
#define WHITE 7
// color text and Black bacground

#define CYAN_T 8
#define WHITE_T 9
#define RED_T 10
#define GREEN_T 11 
#define BLUE_T 12

class GUI : public IGUI
{
private:
    WINDOW      *_w;
    const int   _widht;
    const int   _height;
    float       _speed;

    GUI(const GUI &src);
    GUI& operator = (const GUI &src);

public:
    GUI(int x, int y, float speed); // init
    ~GUI(); // destroi window

    int     pressed_button(); // return buttons id
    void    draw(std::vector<std::pair< std::pair<int, const char*>, std::pair<int, int> > > src);
    void    getXY(int &x, int &y);
    void    change_speed(void);
};

extern "C" {
    IGUI*    createGui(int x, int y, float speed);
    void    destroieGui(IGUI *src);
}

#endif