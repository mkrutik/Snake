#include "GUI.hpp"
#include <dlfcn.h>

GUI::GUI(int x, int y, float update_speed) : _w(), _widht(x), _height(y), _speed(update_speed)
{
    _w = initscr();
    // _w = newwin(y, x, 0, 0);
    wresize(_w, y,x);

    cbreak();
    noecho();
    curs_set(0);

    wrefresh(_w);
    keypad(_w, true);
    nodelay(_w,true);

    start_color();
    halfdelay(_speed);

    init_pair(RED, COLOR_WHITE, COLOR_RED);
    init_pair(GREEN, COLOR_WHITE, COLOR_GREEN);
    init_pair(YELLOW, COLOR_WHITE, COLOR_YELLOW);
    init_pair(BLUE, COLOR_WHITE, COLOR_BLUE);
    init_pair(MAGENTA, COLOR_WHITE, COLOR_MAGENTA);
    init_pair(CYAN, COLOR_WHITE, COLOR_CYAN);
    init_pair(WHITE, COLOR_BLACK, COLOR_WHITE);

    init_pair(RED_T, COLOR_RED, COLOR_BLACK);
    init_pair(GREEN_T, COLOR_GREEN, COLOR_BLACK);
    // init_pair(YELLOW_T, COLOR_YELLOW, COLOR_BLACK);
    init_pair(BLUE_T, COLOR_BLUE, COLOR_BLACK);
    init_pair(WHITE_T, WHITE, COLOR_BLUE);

    init_pair(CYAN_T, COLOR_CYAN, COLOR_BLACK); // for background

    wbkgd(_w, COLOR_PAIR(CYAN_T)); // window background
}

GUI::~GUI()
{
    delwin(_w);
    endwin();
}

int     GUI::pressed_button()
{
    int ch = getch();
    if (ch == KEY_LEFT)
        return (0);
    else if (ch == KEY_RIGHT)
        return (1);
    return ch;
}

void    GUI::draw(std::vector<std::pair< std::pair<int, const char*>, std::pair<int, int> > > src)
{
    wclear(_w);

    attron(COLOR_PAIR(CYAN_T));
    wborder(_w, '|', '|', '-', '-', '+', '+', '+', '+'); // general frame

    for (unsigned long i = 0; i < src.size(); i++)
    {
        attron(COLOR_PAIR(src[i].first.first));
        mvwprintw(_w ,src[i].second.second + 1, src[i].second.first + 1, src[i].first.second);
    }

    // int t = (_widht - src.first_line.second.size()) / 2;

    // attron(COLOR_PAIR(src.first_line.first)); // print first line
    // for (int i = 0; t < _widht && i <src.first_line.second.size(); i++)
    // {
    //     mvwaddch(_w, 1, t++ + 1, src.first_line.second[i]);
    // }

    // attron(COLOR_PAIR(CYAN_T)); // frame after first line
    // for (int i = 0; i < _widht; i++)
    //     mvwprintw(_w, 2, i, "-");


    // for (unsigned long i = 0; i < src.data.size(); i++)
    // {
    //     attron(COLOR_PAIR(src.data[i].first));
    //     mvwprintw(_w ,src.data[i].second.second + 3, src.data[i].second.first + 1, " ");
    // }
    wrefresh(_w);
}

void    GUI::getXY(int &x, int &y)
{
    getmaxyx(_w, y, x);
}

void    GUI::change_speed(void)
{
    _speed -= 0.5;
    halfdelay(_speed);
}

IGUI*    createGui(int x, int y, float speed)
{
    return new GUI(x, y, speed);
}
void    destroieGui(IGUI *src)
{
    delete src;
}