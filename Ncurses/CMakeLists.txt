cmake_minimum_required(VERSION 3.0 FATAL_ERROR)

project(1)

set(CMAKE_CXX_COMPILER_ID Clang)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall -Wextra -Werror -fpic")

file(GLOB_RECURSE SRC ${CMAKE_SOURCE_DIR}/src/*.cpp)
#  main.cpp algorithm.cpp checking.cpp generate_all_things.cpp Zmei.cpp) 

set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_SOURCE_DIR})

add_library(${PROJECT_NAME} SHARED ${SRC})

target_link_libraries(${PROJECT_NAME} "-lncurses")