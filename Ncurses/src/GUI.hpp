#ifndef GUI_HPP
#define GUI_HPP

#include <ncurses.h>
#include "../../game/src/IGUI.hpp"

// bacground color definition with White text color 
#define RED 1 // head
#define GREEN 2 // body
#define YELLOW 3 // food
#define BLUE 4 // food
#define MAGENTA 5 // food
#define CYAN 6 // food
#define WHITE 7 // obstical

#define CYAN_T 8 // bacground

#define WHITE_T 9 // info line

#define RED_T 10 // plain tex text
#define GREEN_T 11 // plain text text
#define BLUE_T 12 // plain text text

class GUI : public IGUI
{
private:
    WINDOW      *_w;
    const int   _widht;
    const int   _height;
    float       _speed;

    GUI(const GUI &src);
    GUI& operator = (const GUI &src);

public:
    GUI(int x, int y, float speed); // init
    ~GUI(); // destroi window

    int     pressed_button(); // return buttons id
    void    draw(std::vector<std::pair< std::pair<int, const char*>, std::pair<int, int> > > src);
    void    getXY(int &x, int &y);
    void    change_speed(void);
};

extern "C" {
    IGUI*    createGui(int x, int y, float speed);
    void    destroieGui(IGUI *src);
}

#endif