#include "GUI.hpp"
#include <dlfcn.h>

GUI::GUI(int x, int y, float update_speed) : _w(), _widht(x * 2 + 2), _height(y + 3), _speed(update_speed)
{
    _w = initscr();
    wresize(_w, _height,_widht);

    cbreak();
    noecho();
    curs_set(0);

    wrefresh(_w);
    keypad(_w, true);
    nodelay(_w,true);

    start_color();
    halfdelay(_speed);

    init_pair(RED, COLOR_WHITE, COLOR_RED);
    init_pair(GREEN, COLOR_WHITE, COLOR_GREEN);

    init_pair(YELLOW, COLOR_WHITE, COLOR_YELLOW);
    init_pair(BLUE, COLOR_WHITE, COLOR_BLUE);
    init_pair(MAGENTA, COLOR_WHITE, COLOR_MAGENTA);
    init_pair(CYAN, COLOR_WHITE, COLOR_CYAN);
    init_pair(WHITE, COLOR_BLACK, COLOR_WHITE);

    init_pair(CYAN_T, COLOR_CYAN, COLOR_BLACK); // for background

    init_pair(WHITE_T, WHITE, COLOR_BLUE);

    init_pair(RED_T, COLOR_RED, COLOR_BLACK);
    init_pair(GREEN_T, COLOR_GREEN, COLOR_BLACK);
    init_pair(BLUE_T, COLOR_BLUE, COLOR_BLACK);

    wbkgd(_w, COLOR_PAIR(CYAN_T)); // window background
}

GUI::~GUI()
{
    endwin();
}

int     GUI::pressed_button()
{
    int ch = getch();
    if (ch == KEY_LEFT)
        return (0);
    else if (ch == KEY_RIGHT)
        return (1);
    return ch;
}

void    GUI::draw(std::vector<std::pair< std::pair<int, const char*>, std::pair<int, int> > > src)
{
    wclear(_w);

    attron(COLOR_PAIR(CYAN_T));
    wborder(_w, '|', '|', '-', '-', '+', '+', '+', '+'); // general frame

    for (unsigned long i = 0; i < src.size(); i++)
    {
        if (src[i].first.first <= 7) // sprites
        {
            attron(COLOR_PAIR(src[i].first.first));
            mvwprintw(_w ,src[i].second.second + 2, src[i].second.first * 2 + 1, src[i].first.second);
            mvwprintw(_w ,src[i].second.second + 2, src[i].second.first * 2 + 2, src[i].first.second);
        }
        else if (src[i].first.first == 9) // info line
        {
            std::string tmp(src[i].first.second);
            int i = _widht - 2 - tmp.size();
            bool f = false;
            while (i)
            {
                if (f)
                {
                    f = false;
                    tmp += " ";
                }
                else
                {
                    f = true;
                    tmp = " " + tmp;
                }
                --i;
            }
            attron(COLOR_PAIR(src[i].first.first));
            mvwprintw(_w ,1, 1, static_cast<const char*>(tmp.c_str()));
        }
        else // text
        {
            std::string text(src[i].first.second);
            attron(COLOR_PAIR(src[i].first.first));
            if (text.size() < static_cast<unsigned long>(_widht))
                mvwprintw(_w ,src[i].second.second + 1, (_widht - 2) / 2 - (text.size() / 2), src[i].first.second);
            else
                mvwprintw(_w ,src[i].second.second + 1, 0, src[i].first.second);

        }
    }
    wrefresh(_w);
}

void    GUI::getXY(int &x, int &y)
{
    getmaxyx(_w, y, x);
}

void    GUI::change_speed(void)
{
    _speed -= 0.5;
    halfdelay(_speed);
}

IGUI*    createGui(int x, int y, float speed)
{
    return new GUI(x, y, speed);
}
void    destroieGui(IGUI *src)
{
    delete src;
}