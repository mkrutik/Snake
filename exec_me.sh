#!/bin/bash

OS="`uname`"

install_for_ubuntu()
{
    NCURSES=$(ldconfig -p | grep ncurses)
    if [ "$NCURSES" = "" ]
    then
        sudo apt-get install libncurses-dev
    fi

    CMAKE=$(which cmake)
    if [ "$CMAKE" = "*not found" ]
        then
        sudo apt-get install cmake
    fi

    SFML=$(ls -la /usr/include | grep SFML)
    if [ "$SFML" = "" ]
    then
        sudo apt-get install sfml
    fi

}

install_for_mac()
{
    BREW=$(which brew)
    if [ "$BREW" = "" ]
    then
        /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
    fi

    NCURSES=$(ls -la /usr/lib/ | grep ncurses)
    if [ "$NCURSES" = "" ]
    then
        brew install ncurses
    fi

    CMAKE=$(which cmake)
    if [ "$CMAKE" = "" ]
        then
        brew install cmake
    fi

    SFML=$(find ~/  -name "*sfml*")
    if [ "$SFML" = "" ]
    then
        brew install sfml
    fi
}

build()
{
    if [ "$OS" = "Linux" ]
    then
        install_for_ubuntu
    elif [ "$OS" = "Darwin" ]
    then
        install_for_mac
    fi

    mkdir ./game/build
    cd ./game/build
    cmake ..
    make
    cd ../..

    mkdir ./Ncurses/build
    cd ./Ncurses/build
    cmake ..
    make
    cd ../..

    NEW=$(pwd)
    OLD="REPLACE_PATH"
    sed -i -e 's~'$OLD'~'$NEW'~g' SFML/src/GUI.cpp

    mkdir ./SFML/build
    cd ./SFML/build
    cmake ..
    make
    cd ../..
}

del()
{
    rm -rf ./game/build
    rm -f ./game/snake

    rm -rf ./Ncurses/build
    rm -f ./Ncurses/lib1.so
    rm -f ./Ncurses/lib1.dylib

    rm -rf ./SFML/build
    rm -f ./SFML/lib2.so
    rm -f ./SFML/lib2.dylib
}

usage()
{
    echo "Usage: ./exec_me.sh [build] OR [del] OR [re]"
    echo "[build] - build projects"
    echo "[del] - clear all temporary and binary files"
    echo "[re] - rebuild project"
}


#start here
if [ "$#" != 1 ]
then
    usage
elif [ "$#" = 1 ]
then
    if [ "$1" = "build" ]
        then
        build
    elif [ "$1" = "del" ]
        then
        del
    elif [ "$1" = "re" ]
        then
        del
        build
    else
        usage
    fi
fi